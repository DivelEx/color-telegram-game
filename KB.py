# -*- coding: utf-8 -*-

import telebot
from telebot import types



######################################################## # begin

def begin():
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    begin = types.KeyboardButton('Начать')

    markup.add(begin)

    return markup

# begin ########################################################
############################################## # color_selection

def color_selection():

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    white = types.KeyboardButton('Белый')
    yellow = types.KeyboardButton('Желтый')
    orange = types.KeyboardButton('Оранжевый')
    red = types.KeyboardButton('Красный')
    pink = types.KeyboardButton('Розовый')
    violet = types.KeyboardButton('Фиолетовый')
    blue = types.KeyboardButton('Синий')
    green = types.KeyboardButton('Зеленый')
    brown = types.KeyboardButton('Коричневый')
    gray = types.KeyboardButton('Серый')
    black = types.KeyboardButton('Черный')

    markup.add(white)
    markup.add(yellow)
    markup.add(orange)
    markup.add(red)
    markup.add(pink)
    markup.add(violet)
    markup.add(blue)
    markup.add(green)
    markup.add(brown)
    markup.add(gray)
    markup.add(black)


    return markup

# color_selection ##############################################



############################################# # gender_selection

def gender_selection():

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    male = types.KeyboardButton('Мужчина')
    female = types.KeyboardButton('Женщина')

    markup.add(male, female)

    return markup

# gender_selection #############################################



############################################## # third_selection

def third_selection():

    markup = types.ReplyKeyboardMarkup()

    first = types.KeyboardButton('Вы видите рядом с собой фигуру, пол которой определить затрудняетесь. Похоже, вы вместе, и вы счастливы.')
    second = types.KeyboardButton('Вы видите только себя и никого больше. Вы абсолютно всё и ничто. С началом, но без конца. Вам это по нраву.')

    markup.add(first)
    markup.add(second)

    return markup

# third_selection ##############################################



# fourth_selection #############################################

def fourth_selection():

    markup = types.ReplyKeyboardMarkup()

    first = types.KeyboardButton('Вы положительно киваете, видя рядом с вами появляющиеся размытое представление не человеческой фигуры, а огромного семени')
    second = types.KeyboardButton('Вы отрицательно машете головой и никто новый рядом не появляется.')

    markup.add(first)
    markup.add(second)

    return markup

############################################# # fourth_selection

# life #########################################################

def life():

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    quiet_life = types.KeyboardButton('Тихая жизнь')
    creative_life = types.KeyboardButton('Творческая жизнь')
    businessman = types.KeyboardButton('Бизнесмен')
    scientist = types.KeyboardButton('Ученый')

    markup.add(quiet_life)
    markup.add(creative_life)
    markup.add(businessman)
    markup.add(scientist)

    return markup

######################################################### # life

# end ##########################################################

def end():

    markup = types.ReplyKeyboardMarkup()

    end = types.KeyboardButton('Вы покорно соглашаетесь и пытаетесь подойти к любой сфере.')

    markup.add(end)

    return markup

########################################################## # end
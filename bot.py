# -*- coding: utf-8 -*-

import telebot
import config
import KB
import messages
import random
###############

from telebot import types

bot = telebot.TeleBot(config.TOKEN)



##################################################################################################
##################################################################################################



@bot.message_handler(commands=['start'])
def welcome(message):

    bot.send_message(message.chat.id, 'Начать', reply_markup=KB.begin())


@bot.message_handler(commands=['rnd2'])
def rand(message):
    bot.send_message(message.chat.id,  str(random.randint(0, 2)))


@bot.message_handler(commands=['rnd100'])
def rand(message):
    bot.send_message(message.chat.id,  str(random.randint(0, 100)))


################################################################

@bot.message_handler(content_types=['text'])

def game(message):

    print(message.text)

    if message.text == 'Начать':
        bot.send_message(message.chat.id, "- А, это опять ты.", reply_markup=KB.color_selection())
        bot.send_message(message.chat.id, "- Что тебя вновь привело сюда? И, как я вижу, твой цвет опять сменился.", reply_markup=None)


    if message.text == 'Белый':
        bot.send_message(message.chat.id, messages.colors['Белый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Желтый':
        bot.send_message(message.chat.id, messages.colors['Желтый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Оранжевый':
        bot.send_message(message.chat.id, messages.colors['Оранжевый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Красный':
        bot.send_message(message.chat.id, messages.colors['Красный'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Розовый':
        bot.send_message(message.chat.id, messages.colors['Розовый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Фиолетовый':
        bot.send_message(message.chat.id, messages.colors['Фиолетовый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == ('Синий'):
        bot.send_message(message.chat.id, messages.colors['Синий'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Зеленый':
        bot.send_message(message.chat.id, messages.colors['Зеленый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Коричневый':
        bot.send_message(message.chat.id, messages.colors['Коричневый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Серый':
        bot.send_message(message.chat.id, messages.colors['Серый'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])

    if message.text == 'Черный':
        bot.send_message(message.chat.id, messages.colors['Черный'], reply_markup=KB.gender_selection())
        bot.send_message(message.chat.id, messages.after_color['1'])
        bot.send_message(message.chat.id, messages.after_color['2'])



    if message.text == 'Мужчина':
        bot.send_message(message.chat.id, messages.gender_selection['Мужчина'], reply_markup=KB.third_selection())
        bot.send_message(message.chat.id, messages.after_gender_selection['1'])

    if message.text == 'Женщина':
        bot.send_message(message.chat.id, messages.gender_selection['Женщина'], reply_markup=KB.third_selection())
        bot.send_message(message.chat.id, messages.after_gender_selection['1'])



    if message.text == 'Вы видите рядом с собой фигуру, пол которой определить затрудняетесь. Похоже, вы вместе, и вы счастливы.':
        bot.send_message(message.chat.id, messages.third_selection['1'], reply_markup=KB.fourth_selection())

    if message.text == 'Вы видите только себя и никого больше. Вы абсолютно всё и ничто. С началом, но без конца. Вам это по нраву.':
        bot.send_message(message.chat.id, messages.third_selection['1'], reply_markup=KB.fourth_selection())



    if message.text in 'Вы положительно киваете, видя рядом с вами появляющиеся размытое представление не человеческой фигуры, а огромного семени':
        bot.send_message(message.chat.id, messages.fourth_selection['1'], reply_markup=KB.life())

    if message.text == 'Вы отрицательно машете головой и никто новый рядом не появляется.':
        bot.send_message(message.chat.id, messages.fourth_selection['1'], reply_markup=KB.life())



    if message.text == 'Тихая жизнь':
        bot.send_message(message.chat.id, messages.life_selection['Тихая жизнь'])
        bot.send_message(message.chat.id, messages.after_life_selection['1'])
        bot.send_message(message.chat.id, messages.after_life_selection['2'])
        bot.send_message(message.chat.id, messages.after_life_selection['3'])
        bot.send_message(message.chat.id, messages.after_life_selection['4'])
        bot.send_message(message.chat.id, messages.after_life_selection['5'])
        bot.send_message(message.chat.id, messages.after_life_selection['6'], reply_markup=KB.end())


    if message.text == 'Творческая жизнь':
        bot.send_message(message.chat.id, messages.life_selection['Творческая жизнь'])
        bot.send_message(message.chat.id, messages.after_life_selection['1'])
        bot.send_message(message.chat.id, messages.after_life_selection['2'])
        bot.send_message(message.chat.id, messages.after_life_selection['3'])
        bot.send_message(message.chat.id, messages.after_life_selection['4'])
        bot.send_message(message.chat.id, messages.after_life_selection['5'])
        bot.send_message(message.chat.id, messages.after_life_selection['6'], reply_markup=KB.end())

    if message.text == 'Бизнесмен':
        bot.send_message(message.chat.id, messages.life_selection['Бизнесмен'])
        bot.send_message(message.chat.id, messages.after_life_selection['1'])
        bot.send_message(message.chat.id, messages.after_life_selection['2'])
        bot.send_message(message.chat.id, messages.after_life_selection['3'])
        bot.send_message(message.chat.id, messages.after_life_selection['4'])
        bot.send_message(message.chat.id, messages.after_life_selection['5'])
        bot.send_message(message.chat.id, messages.after_life_selection['6'], reply_markup=KB.end())

    if message.text == 'Ученый':
        bot.send_message(message.chat.id, messages.life_selection['Ученый'])
        bot.send_message(message.chat.id, messages.after_life_selection['1'])
        bot.send_message(message.chat.id, messages.after_life_selection['2'])
        bot.send_message(message.chat.id, messages.after_life_selection['3'])
        bot.send_message(message.chat.id, messages.after_life_selection['4'])
        bot.send_message(message.chat.id, messages.after_life_selection['5'])
        bot.send_message(message.chat.id, messages.after_life_selection['6'], reply_markup=KB.end())

    

    if message.text == 'Вы покорно соглашаетесь и пытаетесь подойти к любой сфере.':
        rnd = random.randint(0, 1)
        #print(rnd)
        if rnd == 1:
            print(1)
            bot.send_message(message.chat.id, messages.end['1'])
            bot.send_message(message.chat.id, messages.end['12'])
            bot.send_message(message.chat.id, messages.end['13'])
            bot.send_message(message.chat.id, messages.end['14'])
            bot.send_message(message.chat.id, messages.end['15'], reply_markup=KB.begin())
        if rnd == 0:
            print(0)
            bot.send_message(message.chat.id, messages.end['2'])
            bot.send_message(message.chat.id, messages.end['22'])
            bot.send_message(message.chat.id, messages.end['23'])








# RUN
bot.polling(none_stop=True)
